
exports.up = function(knex) {
    return knex.schema
    .createTable('users', table => {
       table.increments('id').unsigned().primary()
       table.string('email').unique().notNullable()
       table.string('password')
       table.timestamp('created_at').defaultTo(knex.fn.now())
    })
}
exports.down = function(knex) {
    return knex.schema
      .dropTable('users')
  
};
