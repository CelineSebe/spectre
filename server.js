const feathers = require('@feathersjs/feathers')
const express = require('@feathersjs/express')

const bodyParser = require('body-parser');
const path = require('path')

const knex = require('knex');
const knexService = require('feathers-knex')
const config = require('./knexfile.js');
const nodemailer = require("nodemailer");
const bcrypt = require('bcrypt')
const saltRounds = 10

const jwt = require('jsonwebtoken')
const authService = require('@feathersjs/authentication');
const cookieParser = require('cookie-parser');
const { response } = require('@feathersjs/express');

// import { sendMail } from './mail.js';
// const { sendMail } = require('mail')

const app = express(feathers())
const spectre = knex(config.development);
const usersService = knexService({Model: spectre, name:'users'})

const mailConfig = {
  host : "smtp.online.net",
  port : 587,
  secure : false,
  auth : {
      user: "fullstack@shdl.fr",
      pass: "Eurek@31",
  },
  name : "shdl.fr",
}

  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  const transporter= nodemailer.createTransport(mailConfig)


/// Turn on JSON parser for REST services
app.use(express.json())
// Turn on URL-encoded parser for REST services
app.use(express.urlencoded({ extended: true }))
// Enable REST services
app.configure(express.rest())

// app.use(bodyParser.json());
app.set('view engine', 'ejs')

app.get('/', function(request, response){
  response.render('signin')
})

app.get('/signin', function(request, response){
    response.render('signin')
  })

app.get('/signup', function(request, response){
  response.render('signup')
})

app.get('/signin-error', function(request, response){
  response.render('signin-error')
})

app.get('/signup-error', function(request, response){
  response.render('signup-error')
})

app.get('/forgotten-password', function(request, response){
  response.render('forgotten-password')
})

app.get('/alter-password/:id', function(request, response) {
  const id = request.params.id
  response.render('alter-password')
})

app.get('/reset-password/', function(request, response) {
  response.render('reset-password')
})

app.post('/login', async function (request, response) {
   
      const users_id = request.body.users_id
      const email = request.body.email
      const password = request.body.password
      const userEmail = await spectre.select('*').from('users').where({'email': email})
      // const {email, password } = request.body
      // const usersList = await spectre.find({ query: email})
      // const usersList = await spectre.select('*').from('users')

      // console.log("usersList")
      // console.log(userEmail[0].email)
 
        if(userEmail[0].email === email){
          const passwordBd = userEmail[0].password
          if(bcrypt.compareSync(password, passwordBd)){
            const token = jwt.sign({'sub': users_id}, "MYSECRET")
            response.cookie("token", token)
            console.log(token)
            response.redirect('/users-list')
          }else{  
            response.render('forgotten-password')
          }
        }else{
          response.redirect('signin-error')
      }
    })

app.post('/create_account', async function(request, response){
  const password = request.body.password
  const hash = bcrypt.hashSync(password, saltRounds)
  const email = request.body.email
  const confirmPassword = request.body.confirmPassword
  const emailDB = await spectre.select('*').from('users').where({'email': email})
    try {
      if(emailDB.length == 0 ){
        if(password === confirmPassword){
          await usersService.create({email, password: hash})
          response.render('signin')
        }else{
          response.redirect('signup-error')
        }
      }
      } catch(err) {
        response.render('signup-error')
      }
    }
)

app.post('/reset-password', async function(request, response){

  // const dotenv = require('dotenv').config;
  const emailUser = request.body.mail
  const id = await spectre.select('id').from('users').where({'email': emailUser})
  // console.log(emailUser)
  const user_id = id[0].id
  console.log(id[0].id)

      // create reusable transporter object using the default SMTP transport
      // send mail with defined transport object
      await transporter.sendMail({
          from: '"Fi Foo dev 👻" <dev@celinesebe.com>', // sender address
          to: emailUser, // list of receivers
          subject: "Reset password✔", // Subject line
          text: "Hello world?", // plain text body
          html: `<p>You requested for a password reset, kindly use this <a href= ${"http://localhost:8080/alter-password/" + user_id}>link</a> to reset your password</p><p>Cheers!</p>` // html body
      })
      response.redirect("/signin")

  
    // console.log("request.body", request.body)
    // const email = request.body.mail
    // try{
    //     console.log(email)
    //     response.render('signin')
    // }catch{
    //   response.status(403)
    // }
})

app.post('/alter-password', async function(request, response){
    try{
    const nwPassword = request.body.mdp
    console.log(nwPassword)
    const newId = await spectre.select('users').from('users').where({id}).update({'password':nwPassword})
    console.log(newId)
    response.render("signin")
    }catch{
      response.status(403).json("mot de passe inchangé")
    }
    
})

//AUTHENTIFICATION
app.use(cookieParser())

app.use(function(request, response, next){
  try{
    const token = request.cookies.token
    const payload = jwt.verify(token, "MYSECRET")
    // request.token = payload
    next()
  }catch(err){
    response.status(400).json("signature invalide")
  }
})

app.get('/users-list', async function(request, response){
  const usersList = await spectre.select('*').from('users')
  response.render('users-list', {usersList})
 
})

app.use('/users', knexService({ Model: spectre, name: 'users'}))

app.listen(8080, function () {
  console.log("Server listening on port 8080")
})